# JS FORM

## Installation
You can download and embed the CSS and Javascript files on your own website. Put the CSS inside the ``<head>`` section and the JS files before the closing ``</body>`` tag.

### CSS
```
<link rel="stylesheet" href="path/to/style.css">
```

### JS
```
<script id="LeapForm" type="application/json">
    {
        "type":"IL",
        "site_url": "https://mywebsite.com",
        "webmaster_id": "85159",
        "subaccount": "0",
        "gc": "true",
        "publisher_id": "34",
        "tier_id": "1",
        "mode": "T"
    }
</script>
<script src="path/to/lf_app.js"></script>
```

### Configuration
Set two global variables for the Javascript.
1.  ``type`` can be set to either ``"IL"`` or ``"PDL"``
2.  ``site_url`` needs to be set to your websites address in order to pass the referrer URL to the API
3.  ``webmaster_id`` needs to be set to your webmaster id / aid
4.  ``subaccount`` optionally set your subaccount number
5.  ``gc`` will show google compliant repost checkbox on the short form
6.  ``publisher_id`` publisher id for cheetahtree
7.  ``tier_id`` tier id for cheetahtree
8.  ``mode`` Designates lead mode: (P) for production, (T) for test


### HTML
You will need to place a tag inside your websites HTML where you want the form to be embedded.
```
<div id="jsform"></div>
```

## CDN
As an alternative method of installation, you can use our CDN instead of hosting the files on your own website.
```
<link rel="stylesheet" href="https://d17whd3psqfdmg.cloudfront.net/bvm-0.1.0/style.css">
```
```
<script src="https://d17whd3psqfdmg.cloudfront.net/bvm-0.1.0/lf_app.js"></script>
```