# JS FORM

## Installation
You can download and embed the CSS and Javascript files on your own website. Put the CSS inside the ``<head>`` section and the JS files before the closing ``</body>`` tag.

### CSS
```
<link rel="stylesheet" href="path/to/style.css">
```

### JS
```
<script id="LeapForm" type="application/json">
    { 
        "type":"IL",
        "site_url": "https://mywebsite.com",
        "webmaster_id": "yourid",
        "webmaster_id2": "yourid",
        "subaccount": ""
    }
</script>
<script src="path/to/lf_app.js"></script>
```

### Configuration
Set two global variables for the Javascript.
1.  ``type`` can be set to either ``"IL"`` or ``"PDL"``
2.  ``site_url`` needs to be set to your websites address in order to pass the referrer URL to the API
3.  ``webmaster_id`` needs to be set to your webmaster id / aid
4.  ``webmaster_id2`` needs to be set to your webmaster id / aid
5.  ``subaccount`` optionally set your subaccount number

### HTML
You will need to place a tag inside your websites HTML where you want the form to be embedded.
```
<div id="jsform"></div>
```

## CDN
As an alternative method of installation, you can use our CDN instead of hosting the files on your own website.
```
<link rel="stylesheet" href="https://d17whd3psqfdmg.cloudfront.net/0.5.0/style.css">
```
```
<script src="https://d17whd3psqfdmg.cloudfront.net/0.5.0/lf_app.js"></script>
```


## Custom Styling
Any CSS style can be overwritten so you can match the colours of the form to your own website branding.
The main thing you'll probably want to change is the color of the button and progress indicator.
To update these do the following...

1.  Open the ``style.css`` file.
2.  Do a find and replace of the value ``#e84e7b`` with the color you want to use.
3.  Save the file.